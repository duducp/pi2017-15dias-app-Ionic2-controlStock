import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import {SQLiteProvider} from '../providers/sq-lite-provider';

import {ListasPage} from '../pages/listas-page/listas-page';


@Component({
  templateUrl: 'app.html' // Template principal
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = null; // Página principal

  pages: Array<{title: string, component: any, icon: string}>; // Pertence ao for de geração do menu automatico

  constructor(
    public platform: Platform,
    public sqliteProvider: SQLiteProvider
  ) {
    this.initializeApp();

    // Irá carregar o menu principal
    this.pages = [
      {title: 'Listas', component: ListasPage, icon: 'list-box'},
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      StatusBar.styleDefault();
      Splashscreen.hide();
      this.sqliteProvider.openDatabase()
        .then(() => this.sqliteProvider.createTableListas())
        .then(() => this.sqliteProvider.createTableProdutos())
        .then(() => {
          this.rootPage = ListasPage;
        })
    });
  }

  // Abre uma página
  openPage(page) {
    this.nav.setRoot(page.component);
  }
}
