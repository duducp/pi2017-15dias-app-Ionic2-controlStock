import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import { MyApp } from './app.component';

import {SQLiteProvider} from '../providers/sq-lite-provider';

import {ListasPage} from '../pages/listas-page/listas-page';
import {ListaProdutosPage} from '../pages/lista-produtos-page/lista-produtos-page';
import {FormProdutoPage} from '../pages/form-produto-page/form-produto-page';

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '6a605570'
  }
};

@NgModule({
  declarations: [
    MyApp,
    ListasPage,
    ListaProdutosPage,
    FormProdutoPage
  ],
  imports: [
    IonicModule.forRoot(MyApp, {
      // https://ionicframework.com/docs/v2/api/config/Config/
      mode: 'md',
      platforms: {
        //Na platform IOS o menuType vai ser overlay, pois por padrão é push. As demais continuara sendo overlay, pois é padrão.
        ios: {
          menuType: 'overlay',
        }
      }
    },
    CloudModule.forRoot(cloudSettings)
    )],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ListasPage,
    ListaProdutosPage,
    FormProdutoPage
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SQLiteProvider
  ]
})
export class AppModule {}
