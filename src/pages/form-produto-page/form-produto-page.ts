import {Component, Input} from '@angular/core';
import {ViewController, ToastController, NavParams} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup} from '@angular/forms';

import {BarcodeScanner} from 'ionic-native';

import {SQLiteProvider} from '../../providers/sq-lite-provider';

@Component({
  selector: 'form-produto-page',
  templateUrl: 'form-produto-page.html',
})
export class FormProdutoPage {

  // Utilizado para inserir valores a um input
  @Input() valueCodBarra: number;
  @Input() valueDescricao: string;
  @Input() valueQuantidade: number;

  // Variáveis
  private myForm: FormGroup;
  private textSubmitButton: string = "Cadastrar";
  private textTitlePage: string = "Cadastrar Produto";
  private produto: any; // Irá guardar todos os dados do produto que será editado
  private tipo: string; // Irá guardar o valor que indica se estamos atualizando ou cadastrando um produto

  constructor(public navParams: NavParams,
              private formBuilder: FormBuilder,
              public viewCtrl: ViewController,
              public sqlProvider: SQLiteProvider,
              public toastCtrl: ToastController) {

    // Grava na variável se irá cadastrar ou editar um produto
    this.tipo = this.navParams.get('tipo');

    // Faz as validações dos inputs
    this.myForm = this.formBuilder.group({
      codBarra: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(13)]],
      descricao: ['', [Validators.required, Validators.maxLength(50)]],
      quantidade: ['', Validators.required]
    });

    // Verifica se está editando um produto
    if (this.tipo === 'update') {
      this.produto = this.navParams.get('produto');

      // Preenche os inputs com os dados corretos
      this.valueCodBarra = this.produto.codBarra;
      this.valueDescricao = this.produto.descricao;
      this.valueQuantidade = this.produto.quantidade;

      // Informa ao validator quais os dados que os inputs tem
      this.myForm.setValue({
        codBarra: this.produto.codBarra,
        descricao: this.produto.descricao,
        quantidade: this.produto.quantidade
      });

      // Desabilita a edição do campo Código de Barras
      this.myForm.get('codBarra').disable(true);

      this.textSubmitButton = "Atualizar";
      this.textTitlePage = "Atualizar Produto";
    }
  }

  // Verifica o tipo do Submit
  submit() {
    if (this.tipo === 'update') {
      this.updateProduto();
    } else if (this.tipo === 'insert') {
      this.insertProduto();
    }
  }

  // Cadastra um produto
  insertProduto() {
    this.sqlProvider.insertProduto(this.myForm.value, this.navParams.get('idLista'))
      .then(() => {
        this.viewCtrl.dismiss();
        let toast = this.toastCtrl.create({
          message: 'Produto cadastrado com sucesso',
          duration: 5000,
          showCloseButton: true,
          closeButtonText: "Ok"
        });
        toast.present();
      })
      .catch(error => {
        console.log(error);
        let toast = this.toastCtrl.create({
          message: 'Erro ao cadastrar o produto',
          duration: 5000,
          showCloseButton: true,
          closeButtonText: "Ok"
        });
        toast.present();
      });
  }

  // Atualiza um produto
  updateProduto() {
    this.sqlProvider.updateProduto(this.myForm.value, this.produto.id)
      .then(() => {
        this.viewCtrl.dismiss();
        let toast = this.toastCtrl.create({
          message: 'Produto atualizado com sucesso',
          duration: 5000,
          showCloseButton: true,
          closeButtonText: "Ok"
        });
        toast.present();
      })
      .catch(error => {
        console.log(error);
        let toast = this.toastCtrl.create({
          message: 'Erro ao atualizar produto',
          duration: 5000,
          showCloseButton: true,
          closeButtonText: "Ok"
        });
        toast.present();
      });
  }

  // Lê um determinado código de barras
  getBarcode() {
    BarcodeScanner.scan({orientation: "landscape"})
      .then((barcodeData) => {
        this.valueCodBarra = barcodeData.text; // Mostra o dado no input
        this.myForm.patchValue({codBarra: barcodeData.text}); // Adiciona a validação do campo
    }, (err) => {
      alert("Falha: " + err);
    });
  }

  //Fecha o modal
  dismiss() {
    this.viewCtrl.dismiss();
  }

}
