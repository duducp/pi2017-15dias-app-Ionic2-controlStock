import {Component} from '@angular/core';
import {
  NavParams,
  ToastController,
  LoadingController,
  ModalController,
  AlertController,
  ItemSliding
} from 'ionic-angular';

import {SQLiteProvider} from '../../providers/sq-lite-provider';

import {FormProdutoPage} from '../form-produto-page/form-produto-page';

@Component({
  selector: 'lista-produtos-page',
  templateUrl: 'lista-produtos-page.html',
})
export class ListaProdutosPage {

  // Variaveis tabela Lista
  idLista: number;
  nomeLista: string;
  descricaoLista: string;

  listas: any[] = [];

  constructor(public navParams: NavParams,
              public toastCtrl: ToastController,
              public loadingCtrl: LoadingController,
              public modalCtrl: ModalController,
              public alertCtrl: AlertController,
              private sqliteProvider: SQLiteProvider) {
    this.idLista = navParams.get('id');
    this.descricaoLista = navParams.get('descricao');
    this.nomeLista = navParams.get('nome');
  }

  /*
   É executado quando a página foi carregada. Esse evento só acontece uma vez por página sendo criada.
   Se uma página sai, mas é armazenada em cache, este evento não será acionado novamente em uma visualização subsequente.
   O evento ionViewDidLoad é um bom lugar para colocar o código de configuração da página.

   https://ionicframework.com/docs/v2/api/navigation/NavController/
   */
  ionViewDidLoad() {
    this.getAllProdutos();
  }

  // Busca todas as listas no BD
  getAllProdutos() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();

    this.sqliteProvider.getProdutos(this.idLista)
      .then(listas => {
        this.listas = listas; // Atribui a variável LISTAS em forma de array, todos os dados das LISTAS buscadas no BD
      })
      .catch(error => {
        let toast = this.toastCtrl.create({
          message: 'Não foi possível buscar os produtos',
          duration: 3000,
          showCloseButton: true,
          closeButtonText: "Ok"
      });
        toast.present();
      });

    loader.dismissAll();
  }

  // Abre a página para cadastrar um novo produto
  insertProduto() {
    let modal = this.modalCtrl.create(FormProdutoPage, {
      idLista: this.idLista,
      tipo: 'insert'
    }, {enableBackdropDismiss: false});
    modal.onDidDismiss(() => {
      this.getAllProdutos();
    });
    modal.present();
  }

  // Abre a página para atualizar o produto
  updateProduto(lista: any) {
    let modal = this.modalCtrl.create(FormProdutoPage, {
      produto: lista,
      tipo: 'update'
    }, {enableBackdropDismiss: false});
    modal.onDidDismiss(() => {
      this.getAllProdutos();
    });
    modal.present();
  }

  // Deleta um produto
  deleteProduto(lista: any, index, slidingItem: ItemSliding) {
    let alert = this.alertCtrl.create({
      title: 'Exclusão',
      message: 'Deseja realmente excluir o produto selecionado?',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            slidingItem.close();
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.sqliteProvider.deleteProduto(lista)
              .then(response => {
                /*
                 Splice eu digo que quero remover do array "lista" apenas "1" item com determinado "index".
                 Assim eu não preciso dar um refresh na página.
                 http://programadorobjetivo.co/javascript-diferenca-slice-vs-splice/
                 */
                this.listas.splice(index, 1);
              })
              .catch(error => {
                let toast = this.toastCtrl.create({
                  message: 'Não foi possível excluir o produto',
                  duration: 3000,
                  showCloseButton: true,
                  closeButtonText: "Ok"
                });
                toast.present();
              })
          }
        }
      ]
    });
    alert.present();
  }
}
