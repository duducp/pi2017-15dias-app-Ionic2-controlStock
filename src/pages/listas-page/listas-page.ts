import {Component} from '@angular/core';
import {AlertController, ToastController, NavController, LoadingController, ItemSliding} from 'ionic-angular';
import {SQLiteProvider} from '../../providers/sq-lite-provider';

import {ListaProdutosPage} from '../lista-produtos-page/lista-produtos-page';

@Component({
  selector: 'listas-page',
  templateUrl: 'listas-page.html'
})
export class ListasPage {

  // Variável
  listas: any[] = [];

  constructor(public alertCtrl: AlertController,
              private sqliteProvider: SQLiteProvider,
              public toastCtrl: ToastController,
              public loadingCtrl: LoadingController,
              public navCtrl: NavController) {
  }

  // Ação que acontece ao carregar a página Home
  ionViewDidLoad() {
    this.getAllListas();
  }

  // Busca todas as listas no BD
  getAllListas() {
    let loader = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loader.present();

    this.sqliteProvider.getAllListas()
      .then(listas => {
        this.listas = listas; // Atribui a variável LISTAS em forma de array, todos os dados das LISTAS buscadas no BD
      })
      .catch((err) => {
        let toast = this.toastCtrl.create({
          message: 'Não foi possível buscar as listas',
          duration: 3000,
          showCloseButton: true,
          closeButtonText: "Ok"
        });
        toast.present();
      });

    loader.dismissAll();
  }

  // Cadastro de nova Lista
  openAlertNewLista() {
    let alert = this.alertCtrl.create({
      title: 'Nova Lista',
      inputs: [
        {
          name: 'nome',
          placeholder: 'Nome',
        },
        {
          name: 'descricao',
          placeholder: 'Descrição',
        }
      ],
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Salvar',
          handler: (data) => {
            if (data.nome.length != 0) {

              this.sqliteProvider.insertLista(data)
                .then(response => {
                  this.getAllListas();
                  //this.listas.unshift(data);
                })
                .catch(error => {
                  console.error(error);
                })

            } else {

              let toast = this.toastCtrl.create({
                message: 'Campo NOME é obrigatório',
                duration: 5000,
                showCloseButton: true,
                closeButtonText: "Ok"
              });
              toast.present();

            }
          }
        }
      ]
    });
    alert.present();
  }

  deleteLista(lista: any, index, slidingItem: ItemSliding) {
    let alert = this.alertCtrl.create({
      title: 'Exclusão',
      message: 'Deseja realmente excluir a lista selecionada?',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            slidingItem.close();
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.sqliteProvider.deleteLista(lista)
              .then(response => {
                /*
                 Splice eu digo que quero remover do array "lista" apenas "1" item com determinado "index".
                 Assim eu não preciso dar um refrash na página.
                 http://programadorobjetivo.co/javascript-diferenca-slice-vs-splice/
                 */
                this.listas.splice(index, 1);
              })
              .catch(error => {
                let toast = this.toastCtrl.create({
                  message: 'Não foi possível excluir a lista',
                  duration: 3000,
                  showCloseButton: true,
                  closeButtonText: "Ok"
                });
                toast.present();
              })
          }
        }
      ]
    });
    alert.present();
  }

  // Refresh Página
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAllListas();
      refresher.complete();
    }, 2000);
  }

  // Abre a página com detalhes da lista
  openLista(lista: any) {
    this.navCtrl.push(ListaProdutosPage, lista);
  }

}
