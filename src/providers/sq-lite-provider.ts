import {Injectable} from '@angular/core';
import {SQLite} from 'ionic-native';
import 'rxjs/add/operator/map';

@Injectable()
export class SQLiteProvider {

  // Variáveis
  db: SQLite = null;

  constructor() {
    this.db = new SQLite();
  }

  // Abre o BD
  openDatabase() {
    return this.db.openDatabase({
      name: 'data.db',
      location: 'default' // the location field is required
    });
  }


  /*
   Métodos relacionados a tabela Listas
   */

  // Cria a tabela de Listas
  createTableListas() {
    let sql = `
      CREATE TABLE IF NOT EXISTS listas(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      nome TEXT NOT NULL,
      descricao TEXT,
      dataCadastro TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      )`;
    return this.db.executeSql(sql, [])
      .then((response) => {

      })
      .catch((err) => {
        console.log(err);
      });
  }

  // Busca todas as Listas
  getAllListas() {
    let sql = 'SELECT * FROM listas ORDER BY id DESC';
    return this.db.executeSql(sql, [])
      .then(response => {
        let listas = [];
        for (let index = 0; index < response.rows.length; index++) {
          listas.push(response.rows.item(index));
        }
        return Promise.resolve(listas);
      })
  }

  // Insere uma Lista ao BD
  insertLista(values: any) {
    let sql = 'INSERT INTO listas(nome, descricao) VALUES(?,?)';
    return this.db.executeSql(sql, [values.nome, values.descricao]);
  }

  // Deleta uma Lista
  deleteLista(value: any) {
    let sql = 'DELETE FROM listas WHERE id=?';
    return this.db.executeSql(sql, [value.id]);
  }


  /*
   Métodos relacionados a tabela Produtos
   */

  // Cria a tabela de Produtos
  createTableProdutos() {
    let sql = `
      CREATE TABLE IF NOT EXISTS produtos(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      codBarra INTEGER NOT NULL,
      descricao TEXT NOT NULL,
      quantidade INTEGER NOT NULL,
      dataCadastro TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      idLista INTEGER,
      FOREIGN KEY(idLista) REFERENCES listas(id),
      CONSTRAINT codbarra_unique UNIQUE (codBarra)
      )`;
    return this.db.executeSql(sql, [])
      .then((response) => {

      })
      .catch((err) => {
        console.log(err);
      });
  }

  // Pega os produtos relacionados a uma lista
  getProdutos(idLista: number) {
    let sql = 'SELECT * FROM produtos WHERE idLista=? ORDER BY id DESC';
    return this.db.executeSql(sql, [idLista])
      .then(response => {
        let listas = [];
        for (let index = 0; index < response.rows.length; index++) {
          listas.push(response.rows.item(index));
        }
        return Promise.resolve(listas);
      })
  }

  // Insere um produto na Lista
  insertProduto(values: any, idLista: any) {
    let sql = 'INSERT INTO produtos(codBarra, descricao, quantidade, idLista) VALUES(?,?,?,?)';
    return this.db.executeSql(sql, [values.codBarra, values.descricao, values.quantidade, idLista]);
  }

  // Atualiza um produto
  updateProduto(values: any, idProduto: number) {
    let sql = 'UPDATE produtos SET descricao=?, quantidade=? WHERE id=?';
    return this.db.executeSql(sql, [values.descricao, values.quantidade, idProduto]);
  }

  // Deleta um produto da Lista
  deleteProduto(value: any) {
    let sql = 'DELETE FROM produtos WHERE id=?';
    return this.db.executeSql(sql, [value.id]);
  }
}
